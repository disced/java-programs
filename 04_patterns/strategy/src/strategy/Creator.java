package strategy;

public class Creator
{

    public static void main ( String[] args )
    {
        // Strategy Pattern.
        Car            c1 = new Car( "BMW" );
        Plane         p1 = new Plane( "Boeing" );
        Submarine s1 = new Submarine( "S301" );
        
        
        c1.moveCar();
        p1.movePlane();
        s1.moveSubmarine();

        System.out.println();
        c1.showName();
        p1.showName();
        s1.showName();
    }

}
