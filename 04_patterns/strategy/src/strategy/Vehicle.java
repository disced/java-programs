package strategy;

public class Vehicle
{
    private String             name;
    protected MovementBehavior moveType;

    public Vehicle ( String name )
    {
        this.name = name;
    }

    public void showName ()
    {
        System.out.println( "The name is: " + name );
    }

}
