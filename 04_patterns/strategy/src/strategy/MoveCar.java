package strategy;

public class MoveCar implements MovementBehavior
{
    public void move ()
    {
        System.out.println( "The car was moved" );
    }

}
