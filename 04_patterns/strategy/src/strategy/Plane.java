package strategy;

public class Plane extends Vehicle
{
    public Plane ( String name )
    {
        super( name );
        moveType = new MovePlane();
    }

    public void movePlane ()
    {
        moveType.move();
    }

}
