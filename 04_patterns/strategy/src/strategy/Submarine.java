package strategy;

public class Submarine extends Vehicle
{
    public Submarine ( String name )
    {
        super( name );
        moveType = new MoveSubmarine();
    }

    public void moveSubmarine ()
    {
        moveType.move();
    }

}
