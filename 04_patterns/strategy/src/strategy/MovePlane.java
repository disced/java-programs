package strategy;

public class MovePlane implements MovementBehavior
{
    public void move ()
    {
        System.out.println( "The plane was moved" );
    }

}
