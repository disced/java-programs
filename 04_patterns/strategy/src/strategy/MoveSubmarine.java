package strategy;

public class MoveSubmarine implements MovementBehavior
{
    public void move ()
    {
        System.out.println( "The submarine was moved" );
    }

}
