package personas;

import java.util.Scanner;

public class Altas {

    static Scanner sc = new Scanner ( System.in );

    public static Profesor nuevoProfesor () {
        String nom, ap, asig, cargo, dni, tlfn;
        int    edad;
        System.out.println ( "Introduce datos para Profesor:" );

        System.out.println ( "Nombre: " );
        nom = sc.nextLine ();

        System.out.println ( "Apellido: " );
        ap = sc.nextLine ();

        System.out.println ( "Edad: " );
        edad = sc.nextInt ();
        sc.nextLine ();

        System.out.println ( "Asignatura: " );
        asig = sc.nextLine ();

        System.out.println ( "Cargo: " );
        cargo = sc.nextLine ();

        System.out.println ( "DNI: " );
        dni = sc.nextLine ();

        System.out.println ( "Telefono: " );
        tlfn = sc.nextLine ();

        Profesor nuevo_profe = new Profesor ( nom, ap, edad, asig, cargo, tlfn, dni );
        return nuevo_profe;
    }

    public static Pas nuevoPas () {
        String nom, ap, puesto, ofi, dni;
        int    horas, edad;
        System.out.println ( "Introduce datos para PAS" );

        System.out.println ( "Nombre:" );
        nom = sc.nextLine ();

        System.out.println ( "Apellido:" );
        ap = sc.nextLine ();

        System.out.println ( "Edad:" );
        edad = sc.nextInt ();
        sc.nextLine ();

        System.out.println ( "Puesto:" );
        puesto = sc.nextLine ();

        System.out.println ( "Oficina:" );
        ofi = sc.nextLine ();

        System.out.println ( "Horas de trabajo:" );
        horas = sc.nextInt ();
        sc.nextLine ();

        System.out.println ( "DNI:" );
        dni = sc.nextLine ();

        Pas nuevo_pas = new Pas ( nom, ap, puesto, ofi, horas, edad, dni );
        return nuevo_pas;
    }

    public static Estudiante nuevoEstudiante () {
        String nom, ap, cur, nia;
        int    ed;
        System.out.println ( "Introduce datos para Estudiante" );

        System.out.println ( "Nombre:" );
        nom = sc.nextLine ();

        System.out.println ( "Apellido:" );
        ap = sc.nextLine ();

        System.out.println ( "Curso:" );
        cur = sc.nextLine ();

        System.out.println ( "NIA:" );
        nia = sc.nextLine ();

        System.out.println ( "Edad:" );
        ed = sc.nextInt ();
        sc.nextLine ();

        Estudiante nuevo_est = new Estudiante ( nom, ap, ed, cur, nia );
        return nuevo_est;
    }

    public static void cierra () {
        sc.close ();
    }

}
