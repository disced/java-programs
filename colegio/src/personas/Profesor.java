package personas;

public class Profesor extends Persona {
    private String asignatura;
    private String cargo;
    private String dni;
    private String tlfn;

    // Constructor con todos los atributos. (generado por eclipse)
    public Profesor ( String nombre, String apellido, int edad, String asignaturas, String cargo, String tlfn,
            String dni ) {
        super ( nombre, apellido, edad );
        boolean rvDni = isDni ( dni );

        if ( rvDni ) {
            this.asignatura = asignaturas;
            this.cargo      = cargo;
            this.dni        = dni;
            this.tlfn       = tlfn;
        } else {
            // Instancio un objeto RuntimeException ex y lo lanzo con throw.
            RuntimeException ex = new RuntimeException ( "DNI NO VALIDO" );
            throw ex;
        }
    }

    public Profesor ( String n, String ap, int ed ) {
        super ( n, ap, ed );
        // No se si esto es muy correcto.
        this.cargo      = "No definido";
        this.asignatura = "No definido";
        this.dni        = "No definido";
    }

    // Getters & Setters
    public String getAsignatura () {
        return asignatura;
    }

    public void setAsignatura ( String asignatura ) {
        this.asignatura = asignatura;
    }

    public String getCargo () {
        return cargo;
    }

    public void setCargo ( String cargo ) {
        this.cargo = cargo;
    }

    public String getDni () {
        return dni;
    }

    public void setDni ( String dni ) {
        this.dni = dni;
    }

    public String getTlfn () {
        return tlfn;
    }

    public void setTlfn ( String tlfn ) {
        this.tlfn = tlfn;
    }

    @Override
    public String toString () {
        return  super.toString () + " Profesor [asignaturas=" + asignatura + ", cargo=" + cargo + ", dni=" + dni + " ";
    }

    private boolean isDni ( String n_dni ) {
        char[]  letras = { 'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V',
                'H', 'L', 'C', 'K', 'K' };
        int     numAscii, i = 0;
        boolean rv     = false;
        int     numero;
        char    letra  = n_dni.toUpperCase ().charAt ( n_dni.length () - 1 );                                       // Guardo la letra y la convierto a mayuscula.

        // Si el string dni es 9 y el ultimo caracter es una letra ejecuta el if.
        if ( n_dni.length () == 9 && Character.isLetter ( letra ) ) {

            do {
                numAscii = n_dni.codePointAt ( i++ );                     // Guardo en numAscii la posición i-ésima del String
                rv       = numAscii > 47 && numAscii < 58 ? true : false; // Compruebo si la posición i-ésima es un numero en la tabla ASCII.
            } while ( i < n_dni.length () - 1 && rv == true ); // Se ejecuta mientras la i sea menor que 8 y rv es true.

            // A lo que devuelve substring le hago un parseInt. substring devuelve un string entre 0 y el tamaño del dni - 1 (para no tener la letra).
            numero = Integer.parseInt ( n_dni.substring ( 0, n_dni.length () - 1 ) );
            if ( letra == letras[numero % 23] && rv )
                rv = true;
            else
                rv = false;
        }
        return rv;
    }

}
