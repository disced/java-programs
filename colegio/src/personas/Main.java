package personas;

import java.util.Scanner;

public class Main {
    static Scanner sc = new Scanner ( System.in );

    public static void main ( String[] args ) throws InterruptedException {

        boolean salir = false;

        System.out.println ( "Dar de alta Profesores, Personal de Administracion o Estudiantes." );
        System.out.println ( "Selecciona una opción del menú:\n\n" );

        while ( !salir ) {
            System.out.print ( "\t1.- Introducir 2 Profesores\n" + "\t2.- Introducir 2 PAS\n"
                    + "\t3.- Introducir 2 Estudiantes\n" + "\t4.- Salir\n" );

            int opc = sc.nextInt ();
            switch ( opc ) {
            case 1:
                // Crear Profesores.
                try {
                    Profesor profe1 = Altas.nuevoProfesor ();
                    Profesor profe2 = Altas.nuevoProfesor ();

                    // Imprime los datos.
                    System.out.print ( "Has creado los siguientes profesores/as:\n\n" );
                    System.out.println ( profe1.toString () + "\n" + profe2.toString () );

                } catch ( Exception e ) {
                    System.out.println ( "Ha habido un fallo al introducir datos \n" );
                    System.out.println ( e.getMessage () );
                    opc = 4;
                }
                break;
            case 2:
                // Crear Personal de Administración.
                try {
                    Pas pas1 = Altas.nuevoPas ();
                    Pas pas2 = Altas.nuevoPas ();

                    // Imprime los datos.
                    System.out.print ( "Has creado dos nuevos Personales de Administración:\n\n" );
                    System.out.println ( pas1.toString () );
                    System.out.println ( pas2.toString () );

                } catch ( Exception e ) {
                    System.out.println ( "Ha habido un fallo al introducir datos \n" );
                    System.out.println ( e.getMessage () );
                    opc = 4;
                }
                break;
            case 3:
                try {
                    Estudiante est1 = Altas.nuevoEstudiante ();
                    Estudiante est2 = Altas.nuevoEstudiante ();

                    System.out.print ( "Has creado dos nuevos Estudiantes:\n\n" );
                    System.out.println ( est1.toString () );
                    System.out.println ( est2.toString () );

                } catch ( Exception e ) {
                    System.out.println ( "Ha habido un fallo al introducir datos \n" );
                    System.out.println ( e.getMessage () );
                    opc = 4;
                }
                break;
            case 4:
                System.out.println ( "Saliendo..." );

                Thread.sleep ( 350 );
                System.out.println ( "Has salido" );

                salir = true;
                break;
            default:
                System.out.print ( "Numero introducido no valido, introduce numeros del 1-4\n\n" );
                break;

            }
            if ( opc > 0 && opc < 4 )
                System.out.print ( "\n\nTienes la posibilidad seguir dando de alta\n\n" );
        }
        sc.close ();
        Altas.cierra ();
    }

}
