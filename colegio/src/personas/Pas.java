package personas;

public class Pas extends Persona {
    private String puesto;
    private String oficina;
    private int    horas_trabajo;
    private String dni;

    // Constructor
    public Pas ( String nombre, String apellido, String puesto, String oficina, int horas_trabajo, int edad,
            String dni ) {
        super ( nombre, apellido, edad );
        boolean rvDni = isDni ( dni );

        if ( rvDni ) {
            this.puesto        = puesto;
            this.oficina       = oficina;
            this.horas_trabajo = horas_trabajo;
            this.dni           = dni;
        } else {
            RuntimeException ex = new RuntimeException ( "DNI NO VALIDO" );
            throw ex;
        }
    }

 // Getters & Setters
    public String getPuesto () {
        return puesto;
    }

    public void setPuesto ( String puesto ) {
        this.puesto = puesto;
    }

    public String getOficina () {
        return oficina;
    }

    public void setOficina ( String oficina ) {
        this.oficina = oficina;
    }

    public int getHoras_trabajo () {
        return horas_trabajo;
    }

    public void setHoras_trabajo ( int horas_trabajo ) {
        this.horas_trabajo = horas_trabajo;
    }

    public String getDni () {
        return dni;
    }

    public void setDni ( String dni ) {
        this.dni = dni;
    }

    @Override
    public String toString () {
        return super.toString () + "Pas puesto=" + puesto + ", oficina=" + oficina + ", horas_trabajo=" + horas_trabajo;
    }

    private boolean isDni ( String n_dni ) {
        char[]  letras = { 'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V',
                'H', 'L', 'C', 'K', 'K' };
        int     numAscii, i = 0;
        boolean rv     = false;
        int     numero;
        char    letra  = n_dni.toUpperCase ().charAt ( 8 );                                                         // Guardo la letra y la convierto a mayuscula.

        // Si el string dni es 9 y el ultimo caracter es una letra ejecuta el if.
        if ( n_dni.length () == 9 && Character.isLetter ( letra ) ) {

            do {
                numAscii = n_dni.codePointAt ( i++ );                     // Guardo en numAscii la posición i-ésima del String
                rv       = numAscii > 47 && numAscii < 58 ? true : false; // Compruebo si la posición i-ésima es un numero en la tabla ASCII.
            } while ( i < n_dni.length () - 1 && rv == true ); // Se ejecuta mientras la i sea menor que 8 y rv es true.

            // A lo que devuelve substring le hago un parseInt. substring devuelve un string entre 0 y el tamaño del dni - 1 (para no tener la letra).
            numero = Integer.parseInt ( n_dni.substring ( 0, n_dni.length () - 1 ) );
            if ( letra == letras[numero % 23] && rv )
                rv = true;
            else
                rv = false;
        }
        return rv;
    }
}
