package personas;

public class Estudiante extends Persona {

    private String curso;
    private String nia;

    // Constructor
    public Estudiante ( String nom, String ap, int ed, String cur, String nia ) {
        super ( nom, ap, ed );
        curso    = cur;
        this.nia = nia;
    }

    // Getters & Setters
    public String getCurso () {
        return curso;
    }

    public void setCurso ( String curso ) {
        this.curso = curso;
    }

    // Si no existiese el metodo toString al hacer por ej. ...println( estudiante );
    // accede al metodo toString de la clase padre.
    @Override
    public String toString () {
        return super.toString () + "Estudiante curso=" + curso + ", nia=" + nia + "]";
    }

}
