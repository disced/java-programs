package juego;

import java.util.Scanner;

public class Pintar {
    static Scanner sc = new Scanner ( System.in );

    public static int pintaMenu ( String[] tipoMenu ) {

        for ( int i = 0; i < tipoMenu.length; i++ )
            System.out.printf ( "%d.- %s\n", i + 1, tipoMenu[i] );
        try {
            return sc.nextInt (); // Retorna la opción elegida.

        } catch ( Exception e ) {
            System.out.println ( "Numero no valido" );
            sc.next ();
            return -1;
        }
    }

    public static void juegoUno ( int opc ) {
        if ( opc < 1 )
            System.out.println ( "Opcion no valida\n\n" );

        else {
            System.out.printf ( "Introduce base:\n" );
            int base = sc.nextInt ();

            switch ( opc ) {
            case 1:
                cuadrado ( base );
                break;
            case 2:
                triangulo ( base );
                break;
            case 3:
                rombo ( base );
                break;
            }
        }
    }

    public static void imprimeLetras ( String palabra ) {
        char let;
        int size = palabra.length ();
        String vocales = "", consonantes = "";

        for ( int i = 0; i < size; i++ ) { // Recorre la palabra.
            let = palabra.charAt ( i ); // Guardar en let la letra i-esima de 'palabra'.
            if ( let > 'a' && let < 'z' ) {
                if ( let == 'a' || let == 'e' || let == 'i' || let == 'o' || let == 'u' )
                    vocales += let; // Guardo las vocales en un String.
                else
                    consonantes += let; // Guardo todo lo demás en un String.
            }
        }
        // Ordeno los objetos String.
        Ordena.ordenaLetras ( vocales );
        Ordena.ordenaLetras ( consonantes );

    }

    private static void cuadrado ( int base ) {
        for ( int i = 0; i < base; i++ ) {
            for ( int j = 0; j < base; j++ ) {
                // Se comprueba si estoy en una fila o columna exterior.
                if ( i == 0 || j == 0 || i == base - 1 || j == base - 1 )
                    System.out.print ( "◘ " );
                else
                    System.out.print ( "  " );
            }
            System.out.printf ( "\n" );
        }
    }

    private static void triangulo ( int base ) {
        for ( int i = 0; i < base; i++ ) {
            for ( int j = 0; j <= i; j++ ) {
                // La diagonal se consigue con la 2ª condición, j == i?
                // la i comienza en 0, luego 1, 2, 3...etc
                if ( j == 0 || j == i || i == base - 1 )
                    System.out.printf ( "◣ " );
                else
                    System.out.printf ( "  " );
            }
            System.out.printf ( "\n" );
        }
    }

    private static void rombo ( int base ) {
        if ( base % 2 == 0 ) // Siempre impar, si no, no sale correcto.
            base++;

        // Una 'piramide', la parte superior
        for ( int i = 1; i <= base; i += 2 ) {
            // Pinta espacios desde la izquiera.
            for ( int j = base + 1; j >= i; j -= 2 )
                System.out.print ( " " );

            // Pinta los asteriscos despues de los espacios.
            for ( int j = 0; j < i; j++ )
                System.out.print ( "▲" );
            System.out.println ();
        }
        // Piramide del revés, la parte inferior.
        // Parecido a la parte superior pero al revés.
        for ( int i = base; i >= 1; i -= 2 ) {
            for ( int j = i; j <= base + 2; j += 2 )
                System.out.print ( " " );

            for ( int j = i - 2; j > 0; j-- )
                System.out.print ( "▼" );
            System.out.println ();
        }
    }
}