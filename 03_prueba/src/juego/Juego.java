package juego;

import java.util.Scanner;

import juego.Pintar;

public class Juego {
    static Scanner sc = new Scanner ( System.in ); // Objeto 'sc' global.
    static Scanner sc2 = new Scanner ( System.in ); // Objeto 'sc2' global.
    // He intentado hacer 'sc.nextLine()' sin nada para escanear un int y despues un
    // String, pero funciona un poco raro, algunas veces tienes que pulsar 2 veces
    // enter otras veces solo 1 vez.

    public static void main ( final String[] args ) {

        // Variables.
        int opc = 0;
        final String[] menuPrincipal = { "Pintar", "Palabras" };
        final String[] menuJuegoUno = { "Cuadrado Vacio", "Triangulo Vacio", "Rombo Relleno" };
        String palabrasJuegoDos;
        boolean sigue = true;

        // Programa.
        while ( sigue == true ) {
            opc = Pintar.pintaMenu ( menuPrincipal ); // Imprime cadena 'menuPrincipal'.

            try {
                switch ( opc ) {
                case 1:
                    opc = Pintar.pintaMenu ( menuJuegoUno );
                    Pintar.juegoUno ( opc );
                    break;
                case 2:
                    System.out.println ( "\nEscribe palabra/s (Minusc. o Mayusc.)" );
                    palabrasJuegoDos = sc2.nextLine ();
                    Pintar.imprimeLetras ( palabrasJuegoDos.replaceAll ( "\\s", "" ).toLowerCase () );
                    // a imprimeLetras le paso lo que devuelve lo que devuelve el metodo toLowerCase
                    // del resultado de replaceAll.
                }
            } catch ( Exception e ) {
                System.out.println ( "Numero no valido" );
            }
            System.out.println ( "\n\n¿Seguir?\n[true/false]\n" );
            sigue = sc.nextBoolean ();
        }

        sc.close ();
        sc2.close ();
    }
}