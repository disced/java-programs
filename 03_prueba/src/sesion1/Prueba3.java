package sesion1;
import java.util.Scanner;

public class Prueba3 {
	public static void main(String[] args) {

		String[] opc = { "Radio", "Altura" };
		double datos[] = { 0, 0 },
				vol;
		final double PI = 3.14159;
		Scanner sc = new Scanner ( System.in );
		

		// Entrada
		System.out.print ( "Volumen Cilindro\nIntroducir datos:\n" );

		for ( int i=0; i<opc.length; i++ ) {
			System.out.println ( "\n" + opc[i] );
			datos[i] = sc.nextDouble();
		}
		
		// Calculo
		// vol = π * rad² * alt;
		vol = PI * Math.pow (datos[0], 2 ) * datos[1];

		// Salida
		System.out.print ( "\n\nCilindro:\nRadio: " + datos[0] + 
						   "\nAltura: "             + datos[1] +
						   "\nVolumen = "           + vol +
						   "\n\n" 
						 );
		sc.close();
	}
}