package sesion1;

import java.util.Scanner;

public class Menu {

    public static void main(String[] args) {

        int base = 3, altura = 4;
        String palabra = "Amad",
                menu = "Opciones\n1.- Area Triangulo\n2.- Area Rectangulo\n3.- Invertir Palabra\n4.- Pinta Cuadrado\n0.- Salir\n\n";
        int opc;
        boolean salir = false;
        Scanner sc = new Scanner(System.in);

        // Resultados de las areas del triangulo y del rectangulo.
        double a_tri, a_rec;

        // Imprime el menú y pide al usuario un Int.
        System.out.printf("%s", menu);
        opc = sc.nextInt();

        // Bucle hasta que se para cuando 'salir' es true.
        do {
            switch (opc) {
            case 0:
                salir = true; // 'salir' es true -> no se cumple la condición del bucle.
                break;
            case 1:
                a_tri = a_triangulo(base, altura);
                System.out.println("Area Triangulo: (" + base + " * " + altura +") / 2 = " + a_tri);
                break;
            case 2:
                a_rec = a_rectangulo(base, altura);
                System.out.println("Area Rectangulo: " + base + " * " + altura + " = " + a_rec);
                break;
            case 3:
                invertir(palabra); // llamada a 'invertir' con 1 parametro.
                break;
            case 4:
                cuadrado(altura);
                break;
            default:
                // Si metemos un numero que no esté entre 0-4 se ejecuta esta opción.
                System.out.printf("Elige opción, 0, 1, 2, 3 o 4\n\n");
            }
            // salir = false | !salir = true
            // se ejecuta si salir negado da true.
            if (!salir) {
                System.out.printf("%s", menu);
                opc = sc.nextInt();
            }
        } while (salir == false);
        sc.close();
    }

    // Retorna el area del triangulo
    private static double a_triangulo(double base, double altura) { return ( base * altura ) / 2; }

    // Retorna el area del rectangulo
    private static double a_rectangulo(double base, double altura) { return base * altura; }

    /*
     * Imprime la palabra en mayusculas y al revés, accediendo a los metodos
     * .toUpperCase() .charAt(i-esima posicion)
     */
    private static void invertir(String palabra) {
        System.out.printf(" %s al revés y mayusculas: ", palabra);

        for (int i = palabra.length() - 1; i >= 0; --i)
            System.out.print(palabra.toUpperCase().charAt(i));
        System.out.print("\n\n");
    }

    // Imprime un cuadrado
    // Imprime por filas, cuando termina con una fila, imprime un \n
    private static void cuadrado(int tam) {
        for (int i = 0; i < tam; i++) {
            for (int j = 0; j < tam; j++)
                System.out.printf("# ");
            System.out.printf("\n");
        }
        System.out.printf("\n\n");
    }
}
